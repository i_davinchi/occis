using Microsoft.AspNetCore.Identity;

namespace OCCIS.Models;

public class Note {
    public int id { get; set; }
    public string title { get; set; }
    public string description { get; set; }
}
