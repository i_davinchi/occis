﻿using Microsoft.AspNetCore.Identity;

namespace OCCIS.Models;

public class ApplicationUser : IdentityUser
{
}
