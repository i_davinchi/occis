import * as React from "react";
import { Outlet } from "react-router-dom";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import style from "./style.module.css";
import TableCell from "@mui/material/TableCell";
import { Link } from "react-router-dom";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
const AddForm = () => {
return(
    <> 
    <Outlet/>
    
        <div className="d-flex flex-row align-items-center justify-content-between ps-0 pe-0 m-0 w-100">
        <span className={`${style["page__heading"]} h2 m-0`}>Employee </span>
        <span className={`${style["add__organization"]}`}>
          <Link
            to="./employeeaddmodal"
            className="btn btn-primary text-light"
          >
            Add Employee
          </Link>
        </span>
      </div>
        
      <div className="w-100 p-3 text-center">
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>No.</TableCell>
                <TableCell align="right">Employee Id</TableCell>
                <TableCell align="right">Employee Name</TableCell>
                <TableCell align="right">Status</TableCell>
                <TableCell align="right">Created At</TableCell>
                <TableCell align="right"></TableCell>
                <TableCell align="right"></TableCell>
                <TableCell align="right"></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
            </TableBody>
          </Table>
        </TableContainer>
              </div>
          
      </>

)

};
export default AddForm;