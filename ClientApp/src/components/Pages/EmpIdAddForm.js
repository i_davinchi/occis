import { useState } from "react";
const EmpIdAddForm = () => {
  const [empIdInput, setEmpIdInput] = useState("");
  const [isActiveInput, setisActiveInput] = useState(true);
 

  const empIdInputHandler = (event) => {
    setEmpIdInput(event.target.value);
  };
  const isActiveInputHandler = (event) => {
    let val = event.target.value === "true" ? true : false;
    setisActiveInput(val);
  };

  const formSubmitHandler = async (event) => {
    event.preventDefault();

  };
  return (
   
      
    <form onSubmit={formSubmitHandler}>
      <label className="form-label" htmlFor="employeeId">
        Employee Id
      </label>
      <input
        type="text"
        className="form-control"
        id="employeeId"
        value={empIdInput}
        onChange={empIdInputHandler}
      />
      <label className="form-label" htmlFor="employeeId">
        Employee name
      </label>
      <input
        type="text"
        className="form-control"
        id="employeeId"
        value={empIdInput}
        onChange={empIdInputHandler}
      />
      <label className="form-label" htmlFor="isActive">
        Is Active
      </label>

      <select
        className="form-select"
        id="isActive"
        aria-label="Default select example"
        onChange={isActiveInputHandler}
        value={isActiveInput}
      >
        <option value={true}>Yes</option>
        <option value={false}>No</option>
      </select>
      <div className="mt-5 d-flex flex-column align-items-end">
        <button className="btn btn-success " type="submit">
          Add employee
        </button>
      </div>
    </form>

  );
};

export default EmpIdAddForm;
